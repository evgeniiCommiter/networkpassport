package utils;

import models.Contact;
import models.Passport;

import java.sql.*;
import java.util.ArrayList;
import java.util.Objects;

public class DB {
    private static Connection connection;

    public static ArrayList<Passport> getPassportsHeaders() {
        ArrayList<Passport> passports = new ArrayList<>();

        try (Statement statement = connection.createStatement()) {
            ResultSet buildingQuery = statement.executeQuery("SELECT b.id, b.name FROM building b");
            while (buildingQuery.next()) {
                passports.add(new Passport(
                        buildingQuery.getLong("id"),
                        buildingQuery.getString("name"),
                        null,
                        null,
                        null,
                        null
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return passports;
    }

    public static ArrayList<Contact> getContacts(long buildingId) {
        ArrayList<Contact> contacts = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet contactQuery = statement.executeQuery(
                    "select c.id, c.name, c.info, ct.name as type " +
                            "from contact c left join contact_type ct " +
                            "on c.type_id = ct.id " +
                            "where c.building_id = " + buildingId
            );
            while (contactQuery.next()) {
                contacts.add(new Contact(
                        contactQuery.getLong("id"),
                        contactQuery.getString("name"),
                        contactQuery.getString("info"),
                        contactQuery.getString("type")
                ));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return contacts;
    }

    public static Passport getPassport(long id) {
        Passport passport = null;

        try (Statement statement = connection.createStatement()) {
            ResultSet buildingQuery = statement.executeQuery("SELECT * FROM building");
            buildingQuery.first();
            long buildingId = buildingQuery.getLong("id");

            passport = new Passport(
                    buildingQuery.getLong("id"),
                    buildingQuery.getString("name"),
                    buildingQuery.getString("address"),
                    buildingQuery.getString("network"),
                    buildingQuery.getString("problems"),
                    getContacts(buildingId)
            );
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return passport;
    }

    public static void updatePassport(Passport passport) throws SQLException {
        String buildingQuery = "UPDATE building SET name = ?, address = ?, network = ?, problems = ? WHERE id = ?";
        String contactQuery = "UPDATE contact SET name = ?, info = ?, type = ? WHERE id = ?";
        try (PreparedStatement buildingStatement = connection.prepareStatement(buildingQuery);
             PreparedStatement contactStatement = connection.prepareStatement(contactQuery);
        ) {
            buildingStatement.setString(1, Objects.toString(passport.getName(), ""));
            buildingStatement.setString(2, Objects.toString(passport.getAddress(), ""));
            buildingStatement.setString(3, Objects.toString(passport.getNetwork(), ""));
            buildingStatement.setString(4, Objects.toString(passport.getProblems(), ""));
            buildingStatement.setLong(5, passport.getId());
            buildingStatement.executeUpdate();

            ArrayList<Contact> contacts = passport.getContacts();
            if (contacts.size() > 0) {
                for (Contact contact : contacts) {
                    contactStatement.setString(1, Objects.toString(contact.getName(), ""));
                    contactStatement.setString(2, Objects.toString(contact.getInfo(), ""));
                    contactStatement.setString(3, Objects.toString(contact.getType(), ""));
                    contactStatement.setLong(4, contact.getId());

                    contactStatement.addBatch();
                }
                contactStatement.executeUpdate();
            }

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        }
    }

    public static void connect(String url, String username, String password)
            throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        closeConnection();
        connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        createTables();
    }

    public static void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void createTables() throws SQLException {
        try (Statement statement = connection.createStatement()) {

            String createBuilding =
                    "CREATE TABLE IF NOT EXISTS building(" +
                            "id int GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY," +
                            "name varchar(255)," +
                            "address varchar(255)," +
                            "network text," +
                            "problems text)";

            String createContact =
                    "CREATE TABLE IF NOT EXISTS contact(" +
                            "id int GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY," +
                            "building_id int REFERENCES building(id) ON DELETE CASCADE," +
                            "type varchar(64)," +
                            "name varchar(64)," +
                            "info text)";

            statement.addBatch(createBuilding);
            statement.addBatch(createContact);
            statement.executeBatch();

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        }
    }
}