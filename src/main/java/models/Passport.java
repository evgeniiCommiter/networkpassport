package models;

import java.io.Serializable;
import java.util.ArrayList;

public class Passport implements Serializable {
    private long id;
    private String name;
    private String address;
    private String network;
    private String problems;
    private ArrayList<Contact> contacts;

    public Passport() {
    }

    public Passport(long id, String name, String address, String network, String problems, ArrayList<Contact> contacts) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.network = network;
        this.problems = problems;
        this.contacts = contacts;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getProblems() {
        return problems;
    }

    public void setProblems(String problems) {
        this.problems = problems;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }
}
