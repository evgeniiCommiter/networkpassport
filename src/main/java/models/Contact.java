package models;

import java.io.Serializable;

public class Contact implements Serializable {
    private long id;
    private String name;
    private String info;
    private String type;

    public Contact() {
    }

    public Contact(long id, String name, String info, String type) {
        this.id = id;
        this.name = name;
        this.info = info;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
